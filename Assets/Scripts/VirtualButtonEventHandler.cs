using UnityEngine;
using UnityEngine.Events;
using Vuforia;

[RequireComponent(typeof(VirtualButtonBehaviour))]
public class VirtualButtonEventHandler : MonoBehaviour
{
    private VirtualButtonBehaviour buttonBehaviour;
    public UnityEvent buttonPressedEvent;
    public UnityEvent buttonReleasedEvent;

    void Start()
    {
        buttonBehaviour = GetComponent<VirtualButtonBehaviour>();
        buttonBehaviour.RegisterOnButtonPressed(OnButtonPressed);
        buttonBehaviour.RegisterOnButtonReleased(OnButtonReleased);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        buttonPressedEvent.Invoke();
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        buttonReleasedEvent.Invoke();
    }
}
