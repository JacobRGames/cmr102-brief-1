using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HSVPicker;

public class SwapCar : MonoBehaviour
{
    [SerializeField] private List<CarsInfo> allCarInfo;
    [SerializeField] private float spinSpeed = 6;
    private EventSystem eventSystem;
    private int carIndex = 0;
    private ColorPicker colorPicker;

    private void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
        colorPicker = FindObjectOfType<ColorPicker>();
        colorPicker.onValueChanged.AddListener(ChangeCarColour);

        for (int i = 0; i < allCarInfo.Count; i++)
        {
            allCarInfo[i].carGameObject.SetActive(false);
        }

        allCarInfo[0].carGameObject.SetActive(true);
        colorPicker.AssignColor(allCarInfo[0].carMaterials[0].color);
    }

    private void FixedUpdate()
    {
        transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y + Time.deltaTime * spinSpeed, transform.localRotation.eulerAngles.z);
    }

    public void SwapToNewCar()
    {
        allCarInfo[carIndex].carGameObject.SetActive(false);

        if (carIndex > allCarInfo.Count - 2)
        {
            carIndex = 0;
            colorPicker.AssignColor(allCarInfo[carIndex].carMaterials[0].color);
            allCarInfo[carIndex].carGameObject.SetActive(true);
            return;
        }
        carIndex++;
        colorPicker.AssignColor(allCarInfo[carIndex].carMaterials[0].color);
        allCarInfo[carIndex].carGameObject.SetActive(true);

        eventSystem.SetSelectedGameObject(null);
    }

    public void ChangeCarColour(Color newColour)
    {
        for (int i = 0; i < allCarInfo[carIndex].carMaterials.Length; i++)
        {
            allCarInfo[carIndex].carMaterials[i].color = newColour;
        }
    }
}

[System.Serializable]
public class CarsInfo
{
    public GameObject carGameObject;
    public Material[] carMaterials;
}
