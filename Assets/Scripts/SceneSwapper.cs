using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SceneSwapper : MonoBehaviour
{
    private TMP_Dropdown dropdown;

    // Start is called before the first frame update
    void Start()
    {
        dropdown = GetComponent<TMP_Dropdown>();
        dropdown.value = SceneManager.GetActiveScene().buildIndex;
        dropdown.onValueChanged.AddListener(SceneChange);

    }

    public void SceneChange(int sceneIndex)
    {
        if (sceneIndex != SceneManager.GetActiveScene().buildIndex)
            SceneManager.LoadScene(sceneIndex);
    }
}
